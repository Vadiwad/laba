package behavioral.strategy.behaviors;

public interface WeaponBehavior {
    void useWeapon();
}
