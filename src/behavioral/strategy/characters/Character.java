package behavioral.strategy.characters;

import behavioral.strategy.behaviors.WeaponBehavior;

public abstract class Character {
    private WeaponBehavior weaponBehavior;

    abstract void fight();

    public void setWeaponBehavior(WeaponBehavior weaponBehavior) {
        this.weaponBehavior = weaponBehavior;
    }
}
